extends Spatial

onready var camera = $Camera
var rotation_speed = PI
var start = 0
var center_view = false
var count = 0 # transition time between current position and target position of camera
var zoom = false
var x_rotation = 0
var y_rotation = 0
var speed_x = 0
var speed_y = 0
var velocity = .5
var is_decrease_speed_y = true
var is_decrease_speed_x = true

var target_x = 0
var target_y = 0

func _ready():
	start = global_transform
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

func _process(delta):

	if is_decrease_speed_y:
		decrease_speed_y()
	else:
		increase_speed_y()
	
	if is_decrease_speed_x:
		decrease_speed_x()
	else:
		increase_speed_x()

	if center_view:
		count += 1
		global_transform = global_transform.interpolate_with(start, .1)
		if count >= 50:
			center_view = false

	else:
		y_rotation = speed_y
		if Input.is_action_pressed("ui_right"):
			speed_y = velocity
			y_rotation += speed_y
			is_decrease_speed_y = true
		if Input.is_action_pressed("ui_left"):
			speed_y = -velocity
			y_rotation += speed_y
			is_decrease_speed_y = false
		rotate_object_local(Vector3.UP, y_rotation * rotation_speed * delta)

		x_rotation = speed_x
		if Input.is_action_pressed("ui_down"):
			speed_x = velocity
			x_rotation += speed_x
			is_decrease_speed_x = true
		if Input.is_action_pressed("ui_up"):
			speed_x = -velocity
			x_rotation += speed_x
			is_decrease_speed_x = false
		rotate_object_local(Vector3.RIGHT, x_rotation * rotation_speed * delta)


	rotate_object_local(Vector3.RIGHT, target_y * .003)
	rotate_object_local(Vector3.UP, target_x * .003)
	target_x = lerp(target_x, 0, .03)
	target_y = lerp(target_y, 0, .03)


	if zoom:
		camera.translate(Vector3(0,0,1))
		if camera.translation.z > 50:
			camera.translation.z = 50
	else:
		camera.translate(Vector3(0,0,-1))
		if camera.translation.z < 8:
			camera.translation.z = 8
		
func _input(event):
	# center view
	if Input.is_key_pressed(KEY_C):
		center_view = true
		count = 0

	# rotation
	if Input.is_action_just_pressed("zoom"):
		zoom = !zoom

	if event is InputEventMouseMotion:
		target_x = event.relative.x
		target_y = event.relative.y

func decrease_speed_y():
	speed_y -= .015
	if speed_y <= 0:
		speed_y = 0

func increase_speed_y():
	speed_y += .015
	if speed_y >= 0:
		speed_y = 0

func decrease_speed_x():
	speed_x -= .015
	if speed_x <= 0:
		speed_x = 0

func increase_speed_x():
	speed_x += .015
	if speed_x >= 0:
		speed_x = 0
