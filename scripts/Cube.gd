extends Spatial

onready var point_mesh_origin = $point_mesh_origin
onready var cube_1 = point_mesh_origin.get_node("cube_1")
onready var face_1 = cube_1.get_node("face_1")
onready var face_2 = cube_1.get_node("face_2")
onready var face_3 = cube_1.get_node("face_3")
onready var face_4 = cube_1.get_node("face_4")
onready var face_5 = cube_1.get_node("face_5")
onready var face_6 = cube_1.get_node("face_6")
onready var point_mesh_1 = cube_1.get_node("point_mesh")
onready var cube_2 = point_mesh_1.get_node("cube_2")
onready var face_25 = cube_2.get_node("face_25")
onready var face_26 = cube_2.get_node("face_26")
onready var face_27 = cube_2.get_node("face_27")
onready var face_28 = cube_2.get_node("face_28")
onready var face_29 = cube_2.get_node("face_29")
onready var face_30 = cube_2.get_node("face_30")
onready var point_mesh_2 = cube_2.get_node("point_mesh")
onready var cube_3 = point_mesh_2.get_node("cube_3")
onready var face_31 = cube_3.get_node("face_31")
onready var face_32 = cube_3.get_node("face_32")
onready var face_33 = cube_3.get_node("face_33")
onready var face_34 = cube_3.get_node("face_34")
onready var face_35 = cube_3.get_node("face_35")
onready var face_36 = cube_3.get_node("face_36")
onready var point_mesh_3 = cube_3.get_node("point_mesh")
onready var cube_4 = point_mesh_3.get_node("cube_4")
onready var face_7 = cube_4.get_node("face_7")
onready var face_8 = cube_4.get_node("face_8")
onready var face_9 = cube_4.get_node("face_9")
onready var face_10 = cube_4.get_node("face_10")
onready var face_11 = cube_4.get_node("face_11")
onready var face_12 = cube_4.get_node("face_12")
onready var point_mesh_4 = cube_4.get_node("point_mesh")
onready var cube_5 = point_mesh_4.get_node("cube_5")
onready var face_19 = cube_5.get_node("face_19")
onready var face_20 = cube_5.get_node("face_20")
onready var face_21 = cube_5.get_node("face_21")
onready var face_22 = cube_5.get_node("face_22")
onready var face_23 = cube_5.get_node("face_23")
onready var face_24 = cube_5.get_node("face_24")
onready var point_mesh_5 = cube_5.get_node("point_mesh")
onready var cube_6 = point_mesh_5.get_node("cube_6")
onready var face_43 = cube_6.get_node("face_43")
onready var face_44 = cube_6.get_node("face_44")
onready var face_45 = cube_6.get_node("face_45")
onready var face_46 = cube_6.get_node("face_46")
onready var face_47 = cube_6.get_node("face_47")
onready var face_48 = cube_6.get_node("face_48")
onready var point_mesh_6 = cube_6.get_node("point_mesh")
onready var cube_7 = point_mesh_6.get_node("cube_7")
onready var face_37 = cube_7.get_node("face_37")
onready var face_38 = cube_7.get_node("face_38")
onready var face_39 = cube_7.get_node("face_39")
onready var face_40 = cube_7.get_node("face_40")
onready var face_41 = cube_7.get_node("face_41")
onready var face_42 = cube_7.get_node("face_42")
onready var point_mesh_7 = cube_7.get_node("point_mesh")
onready var cube_8 = point_mesh_7.get_node("cube_8")
onready var face_13 = cube_8.get_node("face_13")
onready var face_14 = cube_8.get_node("face_14")
onready var face_15 = cube_8.get_node("face_15")
onready var face_16 = cube_8.get_node("face_16")
onready var face_17 = cube_8.get_node("face_17")
onready var face_18 = cube_8.get_node("face_18")

var cs = 2 # one_cube_size
var hcs = cs / 2 # half_cube_size
var angle = 0
var speed_rot = .1
var animation
var animation_list = [0, 1, 2, 3, 4, 5]
var anim = 0
var pos_y = 0
var launch_for_first_time = true
var is_moving = 1
var manip = true
var reverse = false
var yellow = Color(1, .9, 0)
var brown = Color(.7, .4, .2)
var red = Color(1, .1, .1)
var green_light = Color(.3, 1, .3)
var blue_light = Color(.3, .5, 1)
var purple = Color(.6, .1, .6)
var sable = Color(.9, .8, .1)
var dark_blue = Color(.2, .1, .8)
var green_blue = Color(.2, .8, .6)
var dark_pink = Color(.6, .3, .3)

func _ready():
	var list_faces = [face_1, face_2, face_3, face_4, face_5, face_6, face_7, face_8, face_9, face_10, face_11, face_12, face_13, face_14, face_15, face_16, face_17, face_18, face_19, face_20, face_21, face_22, face_23, face_24, face_25, face_26, face_27, face_28, face_29, face_30, face_31, face_32, face_33, face_34, face_35, face_36, face_37, face_38, face_39, face_40, face_41, face_42, face_43, face_44, face_45, face_46, face_47, face_48]
	for face in list_faces:
		face.get_active_material(0).flags_unshaded = true

func angle_rotation():
	is_moving = 1
	angle += speed_rot
	if angle >= deg2rad(180):
		angle = deg2rad(180)
		is_moving = 0
		manip = true

func _physics_process(_delta):
	
	if launch_for_first_time == false:
		animation = animation_list[anim]

	if animation == 0:
		if reverse:
			angle_rotation()
			pos_y += hcs / (deg2rad(180) / speed_rot)
			if pos_y > 0:
				pos_y = 0
				cube_1.rotation.z = deg2rad(0)

			cube_1.global_translate(Vector3(-cs, 0, 0))
			cube_1.transform = cube_1.transform.rotated(Vector3(0, 0, 1), speed_rot * is_moving)
			cube_1.global_translate(Vector3(cs, 0, 0))

			point_mesh_1.rotation.z = deg2rad(180) - angle
			point_mesh_3.rotation.z = deg2rad(180) - angle
			point_mesh_5.rotation.z = deg2rad(-180) + angle
			point_mesh_7.rotation.z = deg2rad(-180) + angle

		else:
			angle_rotation()
			pos_y -= hcs / (deg2rad(180) / speed_rot)
			if pos_y < -hcs:
				pos_y = -hcs
				cube_1.rotation.z = deg2rad(-180)

			cube_1.global_translate(Vector3(-cs, 0, 0))
			cube_1.transform = cube_1.transform.rotated(Vector3(0, 0, -1), speed_rot * is_moving)
			cube_1.global_translate(Vector3(cs, 0, 0))

			point_mesh_1.rotation.z = angle
			point_mesh_3.rotation.z = angle
			point_mesh_5.rotation.z = -angle
			point_mesh_7.rotation.z = -angle

	if animation == 1:
		if reverse:
			angle_rotation()
			pos_y += hcs / (deg2rad(90) / speed_rot)
			if pos_y > -hcs:
				pos_y = -hcs
				cube_1.rotation.x = deg2rad(0)

			cube_1.global_translate(Vector3(0, cs, 0))
			cube_1.transform = cube_1.transform.rotated(Vector3(1, 0, 0), (speed_rot / 2) * is_moving)
			cube_1.global_translate(Vector3(0, -cs, 0))

			point_mesh_4.rotation.x = deg2rad(180) + angle

		else:
			angle_rotation()
			pos_y -= hcs / (deg2rad(90) / speed_rot)
			if pos_y < -3:
				pos_y = -3
				cube_1.rotation.x = deg2rad(-90)

			cube_1.global_translate(Vector3(0, cs, 0))
			cube_1.transform = cube_1.transform.rotated(Vector3(-1, 0, 0), (speed_rot / 2) * is_moving)
			cube_1.global_translate(Vector3(0, -cs, 0))

			point_mesh_4.rotation.x = -angle

	if animation == 2:
		if reverse:
			angle_rotation()
			pos_y += hcs * 3 / (deg2rad(180) / speed_rot)
			if pos_y > -hcs * 3:
				pos_y = -hcs * 3
				cube_1.rotation.x = deg2rad(-90)

			cube_1.global_translate(Vector3(0, 2 * cs, 0))
			cube_1.transform = cube_1.transform.rotated(Vector3(0, 0, 1), (speed_rot / 2) * is_moving)
			cube_1.global_translate(Vector3(0, 2 * -cs, 0))

			point_mesh_2.rotation.y = deg2rad(180) + angle
			point_mesh_6.rotation.y = deg2rad(180) + angle

		else:
			angle_rotation()
			pos_y -= hcs * 3 / (deg2rad(180) / speed_rot)
			if pos_y < -cs * 3:
				pos_y = -cs * 3
				cube_1.rotation.x = deg2rad(0)

			cube_1.global_translate(Vector3(0, 2 * cs, 0))
			cube_1.transform = cube_1.transform.rotated(Vector3(0, 0, -1), (speed_rot / 2) * is_moving)
			cube_1.global_translate(Vector3(0, 2 * -cs, 0))

			point_mesh_2.rotation.y = -angle
			point_mesh_6.rotation.y = -angle

	if animation == 3:
		if reverse:
			angle_rotation()
			pos_y += hcs / (deg2rad(180) / speed_rot)
			if pos_y > -cs * 3:
				pos_y = -cs * 3

			point_mesh_1.rotation.z = angle
			point_mesh_3.rotation.z = angle
			point_mesh_5.rotation.z = -angle
			point_mesh_7.rotation.z = -angle

		else:
			angle_rotation()
			pos_y -= hcs / (deg2rad(180) / speed_rot)
			if pos_y < -hcs * 7:
				pos_y = -hcs * 7

			point_mesh_1.rotation.z = -angle - deg2rad(180)
			point_mesh_3.rotation.z = -angle - deg2rad(180)
			point_mesh_5.rotation.z = angle - deg2rad(180)
			point_mesh_7.rotation.z = angle - deg2rad(180)

	if animation == 4:
		if reverse:
			angle_rotation()
			pos_y += hcs / (deg2rad(90) / speed_rot)
			if pos_y > -hcs * 7:
				pos_y = -hcs * 7
				cube_1.rotation.x = deg2rad(0)

			cube_1.global_translate(Vector3(0, 4 * cs, 0))
			cube_1.transform = cube_1.transform.rotated(Vector3(0, 0, 1), (speed_rot / 2) * is_moving)
			cube_1.global_translate(Vector3(0, 4 * -cs, 0))

			point_mesh_2.rotation.y = -angle
			point_mesh_6.rotation.y = -angle

		else:
			angle_rotation()
			pos_y -= hcs / (deg2rad(90) / speed_rot)
			if pos_y < -hcs * 9:
				pos_y = -hcs * 9
				cube_1.rotation.x = deg2rad(90)

			cube_1.global_translate(Vector3(0, 4 * cs, 0))
			cube_1.transform = cube_1.transform.rotated(Vector3(0, 0, -1), (speed_rot / 2) * is_moving)
			cube_1.global_translate(Vector3(0, 4 * -cs, 0))

			point_mesh_2.rotation.y = angle + deg2rad(180)
			point_mesh_6.rotation.y = angle + deg2rad(180)

	if animation == 5:
		if reverse:
			angle_rotation()
			pos_y += hcs * 3 / (deg2rad(180) / speed_rot)
			if pos_y > -hcs * 9:
				pos_y = -hcs * 9
				cube_1.rotation.x = deg2rad(90)

			cube_1.global_translate(Vector3(0, 5 * cs, 0))
			cube_1.transform = cube_1.transform.rotated(Vector3(1, 0, 0), (speed_rot / 2) * is_moving)
			cube_1.global_translate(Vector3(0, 5 * -cs, 0))

			point_mesh_4.rotation.x = -angle

		else:
			angle_rotation()
			pos_y -= hcs * 3 / (deg2rad(180) / speed_rot)
			if pos_y < -cs * 6:
				pos_y = -cs * 6
				cube_1.rotation.x = deg2rad(0)

			cube_1.global_translate(Vector3(0, 5 * cs, 0))
			cube_1.transform = cube_1.transform.rotated(Vector3(-1, 0, 0), (speed_rot / 2) * is_moving)
			cube_1.global_translate(Vector3(0, 5 * -cs, 0))

			point_mesh_4.rotation.x = angle + deg2rad(180)

	point_mesh_origin.translation.y = pos_y

func _input(_event):
	if manip == true:
		if Input.is_action_just_pressed("ui_page_down"):
			manip = false
			if launch_for_first_time:
				anim = -1
				reverse = false
				launch_for_first_time = false
			angle = 0
			if reverse == false:
				anim += 1
			anim %= len(animation_list)
			if anim == 0:
				pos_y = 0
				cube_1.translation = Vector3(-1, -1, 1)
			reverse = false
		if Input.is_action_just_pressed("ui_page_up"):
			manip = false
			if launch_for_first_time:
				anim = 6
				reverse = true
				launch_for_first_time = false
			angle = 0
			if reverse == true:
				anim -= 1
			if anim < 0:
				anim = len(animation_list) - 1
			if anim == 5:
				pos_y = -cs * 6
				cube_1.translation = Vector3(-1, 11, 1)
			reverse = true
