extends Spatial


onready var pathfollow = $plane/Path/PathFollow
onready var position3d = $plane/Path/PathFollow/Position3D
onready var pathfollow_2 = $plane/Path2/PathFollow
onready var position3d_2 = $plane/Path2/PathFollow/Position3D
onready var sphere = $sphere
var speed = .3
var current_parent = null
var path_num = 1

var elevator_up = false
var direction = Vector3(0,0,0)
var open_path = true

func _ready():
	sphere.transform = position3d.transform
	current_parent = sphere.get_parent()
	current_parent.remove_child(sphere)
	position3d.add_child(sphere)

func _process(delta):
	if open_path:
		if path_num == 1:
			pathfollow.unit_offset += delta * speed
		elif path_num == 2:
			pathfollow_2.unit_offset += delta * speed

	if pathfollow.unit_offset == 1:
		define_current_parent_and_position(sphere, self, position3d)
		pathfollow.unit_offset = 0
		elevator_up = true
		open_path = false
	
	if pathfollow_2.unit_offset == 1:
		define_current_parent_and_position(sphere, self, position3d_2)
		pathfollow_2.unit_offset = 0
		elevator_up = false
		open_path = false

	if elevator_up:
		go_to(position3d_2, sphere)
		if position3d_2.global_transform.origin.distance_to(sphere.global_transform.origin) < .05:
#			sphere.global_transform.origin = position3d_2.global_transform.origin
			define_current_parent_and_position(sphere, position3d_2, position3d_2)
			path_num = 2
			open_path = true
		else:
			sphere.move_and_slide(direction)
	else:
		go_to(position3d, sphere)
		if position3d.global_transform.origin.distance_to(sphere.global_transform.origin) < .05:
#			sphere.global_transform.origin = position3d_2.global_transform.origin
			define_current_parent_and_position(sphere, position3d, position3d)
			path_num = 1
			open_path = true
		else:
			sphere.move_and_slide(direction)

	if Input.is_key_pressed(KEY_Q):
		get_tree().quit()

func go_to(destination, start):
	direction = (destination.global_transform.origin - start.global_transform.origin).normalized() * 2

func define_current_parent_and_position(node, parent, last_position):
	current_parent = node.get_parent()
	current_parent.remove_child(node)
	parent.add_child(node)
	node.global_transform = last_position.global_transform

#func _process(delta):
#	if path_num == 1:
#		pathfollow.unit_offset += delta * speed
#	elif path_num == 2:
#		pathfollow_2.unit_offset += delta * speed
#
#	if pathfollow.unit_offset == 1:
#		sphere.transform = position3d_2.transform
#		current_parent = sphere.get_parent()
#		current_parent.remove_child(sphere)
#		position3d_2.add_child(sphere)
#		pathfollow.unit_offset = 0
#		path_num = 2
#
#	if pathfollow_2.unit_offset == 1:
#		sphere.transform = position3d.transform
#		current_parent = sphere.get_parent()
#		current_parent.remove_child(sphere)
#		position3d.add_child(sphere)
#		pathfollow_2.unit_offset = 0
#		path_num = 1
#
#	if Input.is_key_pressed(KEY_Q):
#		get_tree().quit()
